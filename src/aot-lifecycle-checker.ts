import { DiagnosticCategory } from 'typescript/lib/tsserverlibrary';
import {
    ClassDeclaration,
    ImportDeclaration,
    ImportSpecifier,
    LanguageServiceParser,
    ModuleDeclaration,
    NamespaceImport,
} from '../parser/src';
import { LanguageServiceConstructor } from './language.service';
import * as ts from 'typescript/lib/tsserverlibrary';

interface ResolvedExtendsClassInfo {
    module: ModuleDeclaration,
    class: ClassDeclaration,
}

const LifecycleHooks = [
    'ngOnInit',
    'ngOnDestroy',
];

export function CreateAOTLifecycleHooksChecker(base: LanguageServiceConstructor) {
    class AOTLifecycleHooksCheckerLanguageService extends base {
        private parser = new LanguageServiceParser(this.info.project);

        constructor(private info: ts.server.PluginCreateInfo) {
            super();
        }

        public getSemanticDiagnostics(fileName: string): ts.Diagnostic[] {
            const moduleDeclaration = this.parser.parse(fileName);
            const results: ts.Diagnostic[] = [];
            for (const classDeclaration of moduleDeclaration.classes) {
                const diff = this.analyzeClassDeclaration(classDeclaration, moduleDeclaration);
                if (!diff.length) {
                    continue;
                }
                const start = classDeclaration.rootNode.name.getStart();
                const end = classDeclaration.rootNode.name.getEnd();
                results.push({
                    category: DiagnosticCategory.Error,
                    code: 1,
                    file: moduleDeclaration.sourceFile,
                    start: start,
                    length: end - start,
                    messageText: 'Обнаружено неявное наследование функции жизненного цикла: ' + diff.map(
                        hook => '"' + hook + '"'
                    ).join(', ') + '.\nКомпилятор AOT не видит такие функции.',
                });
            }
            return [
                ...super.getSemanticDiagnostics(fileName),
                ...results,
            ];
        }

        // public getCodeFixesAtPosition(
        //     fileName: string,
        //     start: number,
        //     end: number,
        //     errorCodes: ReadonlyArray<number>,
        //     formatOptions: ts.FormatCodeSettings,
        //     preferences: ts.UserPreferences,
        // ): ReadonlyArray<ts.CodeFixAction> {
        //
        //     const results: Array<ts.CodeFixAction> = [];
        //     if (errorCodes.indexOf(Codes.NotImplementedError) >= 0) {
        //         const moduleDeclaration = this.parser.parse(fileName);
        //         const classDeclaration = moduleDeclaration.findClassAtPosition(start, end);
        //         console.log(classDeclaration);
        //     }
        //     // ts.createLanguageServiceSourceFile()
        //     // const method = ts.createMethod(
        //     //     [],
        //     //     ts.createModifiersFromModifierFlags(ts.ModifierFlags.Public),
        //     //     undefined,
        //     //     'test',
        //     //     undefined,
        //     //     undefined,
        //     //     undefined,
        //     //      undefined,
        //     //        undefined,
        //     //        // ts.createBlock(
        //     //        //     ts.createExpressionStatement(
        //     //        //         ts.createImmediatelyInvokedFunctionExpression()
        //     //        //     )
        //     //        // )
        //     //     );
        //     //
        //     // this.parser.parse(fileName).classes[0].rootNode.
        //     // console.log(method.getText());
        //     return [
        //         ...super.getCodeFixesAtPosition(fileName, start, end, errorCodes, formatOptions, preferences),
        //         ...results,
        //         {
        //             description: 'test test',
        //             changes: [
        //                 {
        //                     fileName: fileName,
        //                     textChanges: [
        //                         {
        //                             span: {
        //                                 start: 0,
        //                                 length: 2,
        //                             },
        //                             newText: 'test test',
        //                         }
        //                     ]
        //                 }
        //             ],
        //             fixName: 'test fix name'
        //         }
        //     ]
        // }

        private analyzeClassDeclaration(
            classDeclaration: ClassDeclaration,
            moduleDeclaration: ModuleDeclaration,
        ): Array<string> {
            const declaredHooks = this.findLifecycleHooks(classDeclaration);
            const extendedHooks = this.analyzeExtendedClassDeclarations(classDeclaration, moduleDeclaration);
            return extendedHooks.filter(hook => declaredHooks.indexOf(hook) < 0);
        }

        private analyzeExtendedClassDeclarations(
            classDeclaration: ClassDeclaration,
            moduleDeclaration: ModuleDeclaration,
        ): Array<string> {
            const result: Array<string> = [];
            const extendedClassInfo = this.resolveExtendedClassInfo(classDeclaration, moduleDeclaration);
            if (extendedClassInfo) {
                result.push(...this.analyzeExtendedClassDeclarations(extendedClassInfo.class, extendedClassInfo.module));
                this.findLifecycleHooks(extendedClassInfo.class).forEach(hook => {
                    if (result.indexOf(hook) < 0) {
                        result.push(hook);
                    }
                });
            }
            return result;
        }

        private resolveExtendedClassInfo(
            classDeclaration: ClassDeclaration,
            moduleDeclaration: ModuleDeclaration,
        ): ResolvedExtendsClassInfo {
            if (!classDeclaration.extendsExpression) {
                return;
            }
            return this.resolveClassDeclarationFile(
                classDeclaration.extendsExpression,
                moduleDeclaration,
            );
        }

        private findLifecycleHooks(classDeclaration: ClassDeclaration): Array<string> {
            return classDeclaration.methods.map(
                methodDeclaration => methodDeclaration.name,
            ).filter(
                methodName => LifecycleHooks.indexOf(methodName) >= 0,
            );
        }

        private resolveClassDeclarationFile(
            className: string,
            moduleDeclaration: ModuleDeclaration,
        ): ResolvedExtendsClassInfo {
            const localClass = moduleDeclaration.classes.find(classDeclaration => classDeclaration.name === className);
            if (localClass) {
                return {
                    module: moduleDeclaration,
                    class: localClass,
                };
            }
            let importDeclaration: ImportDeclaration = undefined;
            let nameBindingElement: ImportSpecifier = undefined;
            for (importDeclaration of moduleDeclaration.imports) {
                if (!importDeclaration.nameBinding || importDeclaration.nameBinding instanceof NamespaceImport) {
                    continue;
                }
                nameBindingElement = importDeclaration.nameBinding.elements.find(
                    element => element.name === className,
                );
                if (nameBindingElement) {
                    break;
                }
            }
            if (!importDeclaration || !nameBindingElement) {
                return;
            }
            const importedModule = this.parser.resolveModuleName(moduleDeclaration.path, importDeclaration.from);
            if (!importedModule) {
                return;
            }
            const dependencyFile = this.parser.parse(importedModule.resolvedFileName);
            return this.resolveClassDeclarationFile(className, dependencyFile);
        }
    }

    return AOTLifecycleHooksCheckerLanguageService;
}
