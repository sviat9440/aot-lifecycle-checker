import * as TypeScript from 'typescript/lib/tsserverlibrary';
import { CreateAOTLifecycleHooksChecker } from './aot-lifecycle-checker';
import { CreateLanguageService } from './language.service';

function init(modules: { typescript: typeof TypeScript }) {
    const ts = modules.typescript;

    function create(info: ts.server.PluginCreateInfo) {
        const LanguageService = CreateLanguageService(info.languageService);
        const AOTLifecycleHooksChecker = CreateAOTLifecycleHooksChecker(LanguageService);
        return new AOTLifecycleHooksChecker(info);
    }

    return { create };
}

export = init;
