import * as ts from 'typescript/lib/tsserverlibrary';

export type LanguageServiceConstructor = new () => ts.LanguageService;
export function CreateLanguageService(base: ts.LanguageService): LanguageServiceConstructor {
    class LanguageService {
    }
    const languageServiceKeys = Object.keys(base) as Array<keyof ts.LanguageService>;
    for (const k of languageServiceKeys) {
        const origin = base[k];
        (LanguageService.prototype as any)[k] = function(...args: Array<{}>) {
            return origin.apply(this, args);
        };
    }
    return LanguageService as any;
}
