export function Safe<T>(cb: () => T, defaultValue: T): T {
    try {
        return cb();
    } catch (e) {
        return defaultValue;
    }
}
