import { ClassDeclaration } from './declarations';
import { Base } from './base';
import * as ts from 'typescript/lib/tsserverlibrary';
import { ImportDeclaration } from './declarations';

export class ModuleDeclaration extends Base<ts.SourceFile> {
    public readonly classes: Array<ClassDeclaration> = this.findRecursive<ts.ClassDeclaration>(
        this.rootNode, ts.SyntaxKind.ClassDeclaration,
    ).map(
        node => new ClassDeclaration(this.sourceFile, node),
    );

    public readonly imports: Array<ImportDeclaration> = this.findRecursive<ts.ImportDeclaration>(
        this.rootNode, ts.SyntaxKind.ImportDeclaration
    ).map(
        node => new ImportDeclaration(this.sourceFile, node),
    );

    public readonly path: string = this.sourceFile.fileName;

    public findClassAtPosition(start: number, end: number): ClassDeclaration {
        return this.classes.find(
            classDeclaration => classDeclaration.rootNode.getStart() <= start && classDeclaration.rootNode.getEnd() >= end
        );
    }

    private findRecursive<T extends ts.Node>(node: ts.Node, kind: ts.SyntaxKind): Array<T> {
        const results: Array<T> = [];
        if (node.kind === kind) {
            results.push(node as any);
        }
        for (const childNode of node.getChildren(this.sourceFile)) {
            results.push(...this.findRecursive(childNode, kind) as any);
        }
        return results;
    }
}
