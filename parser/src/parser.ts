import { ResolvedModule } from 'typescript/lib/tsserverlibrary';
import * as ts from 'typescript/lib/tsserverlibrary';
import { ModuleDeclaration } from './moduleDeclaration';

abstract class Parser {
    abstract getSourceFile(fileName: string): ts.SourceFile;
    abstract resolveModuleName(fileName: string, moduleName: string): ts.ResolvedModule;

    public parse(fileName: string): ModuleDeclaration {
        const file = this.getSourceFile(fileName);
        if (!file) {
            return undefined;
        }
        return new ModuleDeclaration(file, file);
    }
}

export class LanguageServiceParser extends Parser {
    constructor(
        private project: ts.server.Project,
    ) {
        super();
    }

    public getSourceFile(fileName: string): ts.SourceFile {
        return this.project.getSourceFile(this.project.projectService.toPath(fileName));
    }

    public resolveModuleName(fileName: string, moduleName: string): ts.ResolvedModule {
        return this.project.resolveModuleNames([moduleName], fileName)[0];
    }
}

export class TypescriptParser extends Parser {
    constructor(
        private compilerHost: ts.CompilerHost,
        private target: ts.ScriptTarget,
    ) {
        super();
    }

    public getSourceFile(fileName: string): ts.SourceFile {
        return this.compilerHost.getSourceFile(fileName, this.target);
    }

    public resolveModuleName(fileName: string, moduleName: string): ts.ResolvedModule {
        return this.compilerHost.resolveModuleNames([moduleName], fileName)[0];
    }
}
