import { Base } from '../base';
import * as ts from 'typescript/lib/tsserverlibrary';

export class ImportSpecifier extends Base<ts.ImportSpecifier> {
    public readonly name: string = this.rootNode.name.text;
    public readonly propertyName: string = this.rootNode.propertyName
                                           ? this.rootNode.propertyName.text
                                           : this.name;
}

export class NamedImport extends Base<ts.NamedImports> {
    public readonly elements: Array<ImportSpecifier> = this.rootNode.elements.map(
        node => new ImportSpecifier(this.sourceFile, node),
    );
}

export class NamespaceImport extends Base<ts.NamespaceImport> {
    public readonly name = this.rootNode.name.text;
}

export class ImportDeclaration extends Base<ts.ImportDeclaration> {
    public readonly from: string = this.rootNode.moduleSpecifier.getText(this.sourceFile).replace(/['"]/g, '');
    public readonly nameBinding?: NamedImport | NamespaceImport = this.rootNode.importClause && this.rootNode.importClause.namedBindings
                                                                  ? (
                                                                      ts.isNamedImports(this.rootNode.importClause.namedBindings)
                                                                      ?
                                                                      new NamedImport(this.sourceFile, this.rootNode.importClause.namedBindings)
                                                                      :
                                                                      new NamespaceImport(this.sourceFile, this.rootNode.importClause.namedBindings)
                                                                  )
                                                                  : undefined;
}
