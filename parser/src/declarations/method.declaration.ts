import * as ts from 'typescript/lib/tsserverlibrary';
import { Base } from '../base';
import { Safe } from '../decorators';

export enum MethodDeclarationType {
    Public,
    Private,
    Protected,
}


const MethodDeclarationTypeKeywords = [
    ts.SyntaxKind.PublicKeyword,
    ts.SyntaxKind.ProtectedKeyword,
    ts.SyntaxKind.PrivateKeyword,
];

const MethodDeclarationTypePairs: {[key in ts.SyntaxKind]?: MethodDeclarationType} = {
    [ts.SyntaxKind.PrivateKeyword]: MethodDeclarationType.Private,
    [ts.SyntaxKind.ProtectedKeyword]: MethodDeclarationType.Protected,
    [ts.SyntaxKind.PublicKeyword]: MethodDeclarationType.Public,
};

export class MethodDeclaration extends Base<ts.MethodDeclaration> {
    public readonly name: string = this.rootNode.name.getText(this.sourceFile);
    public readonly declarationType: MethodDeclarationType = this.getDeclarationType();

    private getDeclarationType(): MethodDeclarationType {
        const keyword = MethodDeclarationTypeKeywords.find(
            kind => Boolean(
                this.rootNode.modifiers && this.rootNode.modifiers.find(
                    modifier => modifier.kind === kind
                )
            )
        );
        return MethodDeclarationTypePairs[keyword] || MethodDeclarationType.Public;
    }
}
