import { Safe } from '../decorators';
import { Base } from '../base';
import * as ts from 'typescript/lib/tsserverlibrary';
import { MethodDeclaration } from './method.declaration';

export class ClassDeclaration extends Base<ts.ClassDeclaration> {
    public readonly name: string = this.rootNode.name.getText(this.sourceFile);
    public readonly methods: ReadonlyArray<MethodDeclaration> = this.rootNode.members.filter(ts.isMethodDeclaration).map(
        node => new MethodDeclaration(this.sourceFile, node),
    );
    public readonly extendsExpression?: string = Safe(
        () => this.getHeritageClause(ts.SyntaxKind.ExtendsKeyword).types[0].getText(this.sourceFile),
        undefined,
    );
    public readonly isExported: boolean = Safe(
        () => Boolean(
            this.rootNode.getChildren(this.sourceFile).find(
                node => node.getChildAt(0, this.sourceFile).kind === ts.SyntaxKind.ExportKeyword,
            ),
        ),
        false,
    );

    private getHeritageClause(token: ts.SyntaxKind.ExtendsKeyword | ts.SyntaxKind.ImplementsKeyword): ts.HeritageClause {
        return this.rootNode.heritageClauses.find(hc => hc.token === token);
    }
}
