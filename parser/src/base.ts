import * as ts from 'typescript/lib/tsserverlibrary';

export abstract class Base<T extends ts.Node> {
    constructor(
        public sourceFile: ts.SourceFile,
        public rootNode: T,
    ) {
    }
}
