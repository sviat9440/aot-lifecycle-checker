import {
    ClassDeclaration,
    ModuleDeclaration,
    TypescriptParser,
} from '../src';
import { ConfigureParser } from './_configure';
import { expect } from 'chai';

const fileName = __filename.replace('.spec', '');

describe('plain-class', () => {
    let parser: TypescriptParser;
    let moduleInstance: ModuleDeclaration;
    beforeEach(() => {
        parser = ConfigureParser();
        moduleInstance = parser.parse(fileName);
    });

    describe('ModuleInstance', () => {
        describe('get classes', () => {
            it('should return 2 ClassDeclaration', () => {
                expect(moduleInstance.classes).length(2);
            });
        });

        describe('get imports', () => {
            it('should return 0 ImportDeclaration', () => {
                expect(moduleInstance.imports).length(0);
            });
        });

        describe('ClassDeclaration of ExportedPlainClass', () => {
            let classInstance: ClassDeclaration;
            beforeEach(() => {
                classInstance = moduleInstance.classes[0];
            });

            describe('get name', () => {
                it('should return "ExportedPlainClass"', () => {
                    expect(classInstance.name).to.equal('ExportedPlainClass');
                });
            });

            describe('get methods', () => {
                it('should return 0 MethodDeclaration', () => {
                    expect(classInstance.methods).length(0);
                });
            });

            describe('get extendsExpression', () => {
                it('should return undefined', () => {
                    expect(classInstance.extendsExpression).undefined;
                });
            });

            describe('get isExported', () => {
                it('should return true', () => {
                    expect(classInstance.isExported).true;
                });
            });
        });

        describe('ClassDeclaration of NonExportedPlainClass', () => {
            let classInstance: ClassDeclaration;
            beforeEach(() => {
                classInstance = moduleInstance.classes[1];
            });

            describe('get name', () => {
                it('should return "NonExportedPlainClass"', () => {
                    expect(classInstance.name).to.equal('NonExportedPlainClass');
                });
            });

            describe('get methods', () => {
                it('should return 0 MethodDeclaration', () => {
                    expect(classInstance.methods).length(0);
                });
            });

            describe('get extendsExpression', () => {
                it('should return undefined', () => {
                    expect(classInstance.extendsExpression).undefined;
                });
            });

            describe('get isExported', () => {
                it('should return false', () => {
                    expect(classInstance.isExported).false;
                });
            });
        });
    });
});
