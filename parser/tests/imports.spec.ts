import {
    ClassDeclaration,
    ModuleDeclaration,
    ImportDeclaration,
    ImportSpecifier,
    NamedImport,
    NamespaceImport,
    TypescriptParser,
} from '../src';
import { ConfigureParser } from './_configure';
import { expect } from 'chai';

const fileName = __filename.replace('.spec', '');

describe('imports', () => {
    let parser: TypescriptParser;
    let moduleInstance: ModuleDeclaration;
    beforeEach(() => {
        parser = ConfigureParser();
        moduleInstance = parser.parse(fileName);
    });

    describe('ModuleInstance', () => {
        describe('get classes', () => {
            it('should return 0 ClassDeclaration', () => {
                expect(moduleInstance.classes).length(0);
            });
        });

        describe('get imports', () => {
            it('should return 4 ImportDeclaration', () => {
                expect(moduleInstance.imports).length(4);
            });
        });
    });

    describe('ImportDeclaration of { A, B } from "./_exports"', () => {
        let importDeclaration: ImportDeclaration;
        beforeEach(() => {
            importDeclaration = moduleInstance.imports[0];
        });

        describe('get from', () => {
            it('should return "./_exports"', () => {
                expect(importDeclaration.from).to.equal('./_exports');
            });
        });

        describe('get nameBinding', () => {
            it('should return instance of NamedImport', () => {
                expect(importDeclaration.nameBinding).instanceof(NamedImport);
            });

            describe('NamedImport of { A, B }', () => {
                let namedImport: NamedImport;
                beforeEach(() => {
                    namedImport = importDeclaration.nameBinding as NamedImport;
                });

                describe('get elements', () => {
                    it('should return 2 ImportSpecifier', () => {
                        expect(namedImport.elements).length(2);
                    });

                    describe('ImportSpecifier of A', () => {
                        let importSpecifier: ImportSpecifier;
                        beforeEach(() => {
                            importSpecifier = namedImport.elements[0];
                        });

                        describe('get name', () => {
                            it('should return "A"', () => {
                                expect(importSpecifier.name).to.equal('A');
                            });
                        });

                        describe('get propertyName', () => {
                            it('should return "A"', () => {
                                expect(importSpecifier.propertyName).to.equal('A');
                            });
                        });
                    });

                    describe('ImportSpecifier of B', () => {
                        let importSpecifier: ImportSpecifier;
                        beforeEach(() => {
                            importSpecifier = namedImport.elements[1];
                        });

                        describe('get name', () => {
                            it('should return "B"', () => {
                                expect(importSpecifier.name).to.equal('B');
                            });
                        });

                        describe('get propertyName', () => {
                            it('should return "B"', () => {
                                expect(importSpecifier.propertyName).to.equal('B');
                            });
                        });
                    });
                });
            });
        });
    });

    describe('ImportDeclaration of { A as AA, B as BB } from "./_exports"', () => {
        let importDeclaration: ImportDeclaration;
        beforeEach(() => {
            importDeclaration = moduleInstance.imports[1];
        });

        describe('get from', () => {
            it('should return "./_exports"', () => {
                expect(importDeclaration.from).to.equal('./_exports');
            });
        });

        describe('get nameBinding', () => {
            it('should return instance of NamedImport', () => {
                expect(importDeclaration.nameBinding).instanceof(NamedImport);
            });

            describe('NamedImport of { A as AA, B as BB }', () => {
                let namedImport: NamedImport;
                beforeEach(() => {
                    namedImport = importDeclaration.nameBinding as NamedImport;
                });

                describe('get elements', () => {
                    it('should return 2 ImportSpecifier', () => {
                        expect(namedImport.elements).length(2);
                    });

                    describe('ImportSpecifier of A as AA', () => {
                        let importSpecifier: ImportSpecifier;
                        beforeEach(() => {
                            importSpecifier = namedImport.elements[0];
                        });

                        describe('get name', () => {
                            it('should return "AA"', () => {
                                expect(importSpecifier.name).to.equal('AA');
                            });
                        });

                        describe('get propertyName', () => {
                            it('should return "A"', () => {
                                expect(importSpecifier.propertyName).to.equal('A');
                            });
                        });
                    });

                    describe('ImportSpecifier of B as BB', () => {
                        let importSpecifier: ImportSpecifier;
                        beforeEach(() => {
                            importSpecifier = namedImport.elements[1];
                        });

                        describe('get name', () => {
                            it('should return "BB"', () => {
                                expect(importSpecifier.name).to.equal('BB');
                            });
                        });

                        describe('get propertyName', () => {
                            it('should return "B"', () => {
                                expect(importSpecifier.propertyName).to.equal('B');
                            });
                        });
                    });
                });
            });
        });
    });

    describe('ImportDeclaration of * as C from "./_exports"', () => {
        let importDeclaration: ImportDeclaration;
        beforeEach(() => {
            importDeclaration = moduleInstance.imports[2];
        });

        describe('get from', () => {
            it('should return "./_exports"', () => {
                expect(importDeclaration.from).to.equal('./_exports');
            });
        });

        describe('get nameBinding', () => {
            it('should return instance of NamespaceImport', () => {
                expect(importDeclaration.nameBinding).instanceof(NamespaceImport);
            });

            describe('NamedImport of * as C', () => {
                let namespaceImport: NamespaceImport;
                beforeEach(() => {
                    namespaceImport = importDeclaration.nameBinding as NamespaceImport;
                });

                describe('get name', () => {
                    it('should return "C"', () => {
                        expect(namespaceImport.name).to.equal('C');
                    });
                });
            });
        });
    });

    describe('ImportDeclaration of "./_exports"', () => {
        let importDeclaration: ImportDeclaration;
        beforeEach(() => {
            importDeclaration = moduleInstance.imports[3];
        });

        describe('get from', () => {
            it('should return "./_exports"', () => {
                expect(importDeclaration.from).to.equal('./_exports');
            });
        });

        describe('get nameBinding', () => {
            it('should return undefined', () => {
                expect(importDeclaration.nameBinding).undefined;
            });
        });
    });
});
