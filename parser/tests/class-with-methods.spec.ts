import {
    ClassDeclaration,
    ModuleDeclaration,
    ImportDeclaration,
    MethodDeclaration,
    MethodDeclarationType,
    TypescriptParser,
} from '../src';
import { ConfigureParser } from './_configure';
import { expect } from 'chai';

const fileName = __filename.replace('.spec', '');

describe('class-with-methods', () => {
    let parser: TypescriptParser;
    let moduleInstance: ModuleDeclaration;
    beforeEach(() => {
        parser = ConfigureParser();
        moduleInstance = parser.parse(fileName);
    });

    describe('ModuleInstance', () => {
        describe('get classes', () => {
            it('should return 1 ClassDeclaration', () => {
                expect(moduleInstance.classes).length(1);
            });
        });

        describe('get imports', () => {
            it('should return 0 ImportDeclaration', () => {
                expect(moduleInstance.imports).length(0);
            });
        });
    });

    describe('ClassDeclaration of ClassWithMethods', () => {
        let classDeclaration: ClassDeclaration;
        beforeEach(() => {
            classDeclaration = moduleInstance.classes[0];
        });

        describe('get name', () => {
            it('should return "ClassWithMethods"', () => {
                expect(classDeclaration.name).to.equal('ClassWithMethods');
            });
        });

        describe('get methods', () => {
            it('should return 7 MethodDeclaration', () => {
                expect(classDeclaration.methods).length(7);
            });

            describe('MethodDeclaration of simpleMethod', () => {
                let methodDeclaration: MethodDeclaration;
                beforeEach(() => {
                    methodDeclaration = classDeclaration.methods[0];
                });

                describe('get name', () => {
                    it('should return "simpleMethod"', () => {
                        expect(methodDeclaration.name).to.equal('simpleMethod');
                    });
                });

                describe('get declarationType', () => {
                    it('should return MethodDeclarationType.Public', () => {
                        expect(methodDeclaration.declarationType).to.equal(MethodDeclarationType.Public);
                    });
                });
            });

            describe('MethodDeclaration of simpleMethodWithReturnType', () => {
                let methodDeclaration: MethodDeclaration;
                beforeEach(() => {
                    methodDeclaration = classDeclaration.methods[1];
                });

                describe('get name', () => {
                    it('should return "simpleMethodWithReturnType"', () => {
                        expect(methodDeclaration.name).to.equal('simpleMethodWithReturnType');
                    });
                });

                describe('get declarationType', () => {
                    it('should return MethodDeclarationType.Public', () => {
                        expect(methodDeclaration.declarationType).to.equal(MethodDeclarationType.Public);
                    });
                });
            });

            describe('MethodDeclaration of methodWithArguments', () => {
                let methodDeclaration: MethodDeclaration;
                beforeEach(() => {
                    methodDeclaration = classDeclaration.methods[2];
                });

                describe('get name', () => {
                    it('should return "methodWithArguments"', () => {
                        expect(methodDeclaration.name).to.equal('methodWithArguments');
                    });
                });

                describe('get declarationType', () => {
                    it('should return MethodDeclarationType.Public', () => {
                        expect(methodDeclaration.declarationType).to.equal(MethodDeclarationType.Public);
                    });
                });
            });

            describe('MethodDeclaration of methodWithArgumentsAndReturnType', () => {
                let methodDeclaration: MethodDeclaration;
                beforeEach(() => {
                    methodDeclaration = classDeclaration.methods[3];
                });

                describe('get name', () => {
                    it('should return "methodWithArgumentsAndReturnType"', () => {
                        expect(methodDeclaration.name).to.equal('methodWithArgumentsAndReturnType');
                    });
                });

                describe('get declarationType', () => {
                    it('should return MethodDeclarationType.Public', () => {
                        expect(methodDeclaration.declarationType).to.equal(MethodDeclarationType.Public);
                    });
                });
            });

            describe('MethodDeclaration of publicMethod', () => {
                let methodDeclaration: MethodDeclaration;
                beforeEach(() => {
                    methodDeclaration = classDeclaration.methods[4];
                });

                describe('get name', () => {
                    it('should return "publicMethod"', () => {
                        expect(methodDeclaration.name).to.equal('publicMethod');
                    });
                });

                describe('get declarationType', () => {
                    it('should return MethodDeclarationType.Public', () => {
                        expect(methodDeclaration.declarationType).to.equal(MethodDeclarationType.Public);
                    });
                });
            });

            describe('MethodDeclaration of privateMethod', () => {
                let methodDeclaration: MethodDeclaration;
                beforeEach(() => {
                    methodDeclaration = classDeclaration.methods[5];
                });

                describe('get name', () => {
                    it('should return "privateMethod"', () => {
                        expect(methodDeclaration.name).to.equal('privateMethod');
                    });
                });

                describe('get declarationType', () => {
                    it('should return MethodDeclarationType.Private', () => {
                        expect(methodDeclaration.declarationType).to.equal(MethodDeclarationType.Private);
                    });
                });
            });

            describe('MethodDeclaration of protectedMethod', () => {
                let methodDeclaration: MethodDeclaration;
                beforeEach(() => {
                    methodDeclaration = classDeclaration.methods[6];
                });

                describe('get name', () => {
                    it('should return "protectedMethod"', () => {
                        expect(methodDeclaration.name).to.equal('protectedMethod');
                    });
                });

                describe('get declarationType', () => {
                    it('should return MethodDeclarationType.Protected', () => {
                        expect(methodDeclaration.declarationType).to.equal(MethodDeclarationType.Protected);
                    });
                });
            });
        });

        describe('get extendsExpression', () => {
            it('should return undefined', () => {
                expect(classDeclaration.extendsExpression).undefined;
            });
        });

        describe('get isExported', () => {
            it('should return false', () => {
                expect(classDeclaration.isExported).false;
            });
        });
    });
});
