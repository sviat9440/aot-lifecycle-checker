import { ExportedPlainClass } from './plain-class';

export class ExtendedWithExternalClass extends ExportedPlainClass {
}

class NonExportedPlainClass {
}

export class ExtendedWithInternalClass extends NonExportedPlainClass {
}
