class ClassWithMethods {
    simpleMethod() {
    }

    simpleMethodWithReturnType(): void {
    }

    methodWithArguments(a: string, b: number) {
    }

    methodWithArgumentsAndReturnType(a: string, b: number): void {
    }

    public publicMethod() {
    }

    private privateMethod() {
    }

    protected protectedMethod() {
    }
}
