import {
    ClassDeclaration,
    ModuleDeclaration,
    ImportDeclaration,
    TypescriptParser,
} from '../src';
import { ConfigureParser } from './_configure';
import { expect } from 'chai';

const fileName = __filename.replace('.spec', '');

describe('extended-class', () => {
    let parser: TypescriptParser;
    let moduleInstance: ModuleDeclaration;
    beforeEach(() => {
        parser = ConfigureParser();
        moduleInstance = parser.parse(fileName);
    });

    describe('ModuleInstance', () => {
        describe('get classes', () => {
            it('should return 3 ClassDeclaration', () => {
                expect(moduleInstance.classes).length(3);
            });
        });

        describe('get imports', () => {
            it('should return 1 ImportDeclaration', () => {
                expect(moduleInstance.imports).length(1);
            });
        });

        describe('ClassDeclaration of ExtendedWithExternalClass', () => {
            let classInstance: ClassDeclaration;
            beforeEach(() => {
                classInstance = moduleInstance.classes[0];
            });

            describe('get name', () => {
                it('should return "ExtendedWithExternalClass"', () => {
                    expect(classInstance.name).to.equal('ExtendedWithExternalClass');
                });
            });

            describe('get methods', () => {
                it('should return 0 MethodDeclaration', () => {
                    expect(classInstance.methods).length(0);
                });
            });

            describe('get extendsExpression', () => {
                it('should return "ExportedPlainClass"', () => {
                    expect(classInstance.extendsExpression).to.equal('ExportedPlainClass');
                });
            });

            describe('get isExported', () => {
                it('should return true', () => {
                    expect(classInstance.isExported).true;
                });
            });
        });

        describe('ClassDeclaration of ExtendedWithInternalClass', () => {
            let classInstance: ClassDeclaration;
            beforeEach(() => {
                classInstance = moduleInstance.classes[2];
            });

            describe('get name', () => {
                it('should return "ExtendedWithInternalClass"', () => {
                    expect(classInstance.name).to.equal('ExtendedWithInternalClass');
                });
            });

            describe('get methods', () => {
                it('should return 0 MethodDeclaration', () => {
                    expect(classInstance.methods).length(0);
                });
            });

            describe('get extendsExpression', () => {
                it('should return undefined', () => {
                    expect(classInstance.extendsExpression).to.equal('NonExportedPlainClass');
                });
            });

            describe('get isExported', () => {
                it('should return true', () => {
                    expect(classInstance.isExported).true;
                });
            });
        });

        describe('ImportDeclaration of ./plain-class', () => {
            let importDeclaration: ImportDeclaration;
            beforeEach(() => {
                importDeclaration = moduleInstance.imports[0];
            });

            describe('get from', () => {
                it('should return "./plain-class"', () => {
                    expect(importDeclaration.from).to.equal('./plain-class');
                });
            });
        });
    });
});
