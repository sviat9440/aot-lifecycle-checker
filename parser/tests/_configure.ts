import { TypescriptParser } from '../src/parser';
import * as ts from 'typescript/lib/tsserverlibrary';

export function ConfigureParser(): TypescriptParser {
    const compiler = ts.createCompilerHost({});
    return new TypescriptParser(compiler, ts.ScriptTarget.Latest);
}
